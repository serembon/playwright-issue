const { expect } = require("@playwright/test");
const { test } = require("./fixtures");

process.env.DEBUG = "pw:browser*";

test("popup page", async ({ page }) => {
  await page.goto(`https://playwright.dev`);
  expect(await page.title()).toContain('Playwright');
});
