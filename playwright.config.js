const config = {
  testDir: "./tests",
  timeout: 30 * 1000,
  expect: {
    timeout: 15000,
  },
  fullyParallel: true,
  forbidOnly: !!process.env.CI,
  workers: 2,
  use: {
    actionTimeout: 0,
    trace: "on-first-retry",
  },
};

module.exports = config;
